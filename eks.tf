module "eks" {
  source           = "terraform-aws-modules/eks/aws"
  cluster_name     = "gitops-demo-eks"
  cluster_version  = "1.17"
  subnets          = module.vpc.public_subnets
  write_kubeconfig = "false"
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
  vpc_id = module.vpc.vpc_id

  node_groups = {
    example = {
      desired_capacity = 5
      max_capacity     = 7
      min_capacity     = 1

      instance_type = "m4.large"
      k8s_labels = {
        Environment = "dev"
        Terraform   = "true"
      }
      tags = {
        "k8s.io/cluster-autoscaler/gitops-demo-eks" = "owned"
        "k8s.io/cluster-autoscaler/enabled"         = "true"
      }
    }
  }
}

output "env-dynamic-url" {
  value = module.eks.cluster_endpoint
}

output "node_groups" {
  description = "Outputs from node groups"
  value       = module.eks.node_groups
}
